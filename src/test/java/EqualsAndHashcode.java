import org.junit.Test;

import java.util.Objects;

import static org.junit.Assert.*;

public class EqualsAndHashcode {

    @Test
    public void collisionExample(){
        String first = "Aa";
        String second = "BB";
        assertEquals(first.hashCode(), second.hashCode());
        assertFalse(first.equals(second));

        first ="somePrefix_DB";
        second = "somePrefix_Ca";
        assertEquals(first.hashCode(), second.hashCode());
        assertFalse(first.equals(second));
    }

    /**
     * Вложенный класс только что бы не создавать отдельный файл.
     *
     * Equals и hashcode нужно в самом классе из src/main реализовывать.
     */
    static class SomeObject {
        private int anInt;
        private boolean aBoolean;
        private String string;


        @Override
        public boolean equals(Object o) {
            if (this == o) return true;
            if (o == null || getClass() != o.getClass()) return false;
            SomeObject that = (SomeObject) o;
            return anInt == that.anInt && aBoolean == that.aBoolean && string.equals(that.string);
        }

        @Override
        public int hashCode() {
            return Objects.hash(anInt, aBoolean, string);
        }
    }
}
