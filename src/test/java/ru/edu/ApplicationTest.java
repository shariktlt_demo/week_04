package ru.edu;

import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import ru.edu.model.Athlete;
import ru.edu.screens.MainScreen;
import ru.edu.screens.Screen;

import java.io.IOException;
import java.io.InputStream;
import java.io.PrintStream;

import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.*;
import static org.mockito.MockitoAnnotations.openMocks;

public class ApplicationTest {

    @Mock
    private InputStream in;

    @Mock
    private PrintStream out;

    @Mock
    private Screen mainScreen;

    @Mock
    private Competition competition;

    Application app;

    @Before
    public void setup(){
        openMocks(this);
        app = new Application(in, out, competition, mainScreen);
    }


    /**
     * Для случая бесконечного цикла, завершим тест по таймауту
     */
    @Test(timeout = 1000)
    public void run(){
        when(mainScreen.readInput(competition)).thenReturn(mainScreen, null);

        when(competition.register(any(Athlete.class))).thenThrow(new IllegalArgumentException("mock excpetion"));

        app.run();

        verify(mainScreen, times(2)).promt();
        verify(mainScreen, times(2)).readInput(competition);
    }


    //    @Test
    public void runExample() throws IOException {
        when(in.read()).thenReturn(123);

        app.run();
        verify(out).println(anyString());
        verify(out).println("Выберите действие:");
        verify(out).println(123);
    }
}