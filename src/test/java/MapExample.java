import org.junit.Test;

import java.util.*;

import static org.junit.Assert.*;

public class MapExample {

    @Test
    public void map(){
        //В качестве ключа у нас класс из библиотеки, с ним проблем нет
        Map<String, Integer> stringIntegerMap = new HashMap<>();


        Map<MyObject, Integer> myObjectMap = new HashMap<>();
        /**
         * Наглядное устройство map:
         * bucket[0]  - [Entry.key, Entry.key, ...]
         * bucket[1] - [Entry.key, Entry.key, ...]
         * ...
         * bucket[n] - [Entry.key, Entry.key, ...]
         */

        MyObject key = new MyObject(1);
        //int oldHashCode = key.hashCode(); //пример без final

        myObjectMap.put(key, 1);
        //key.id = 2; если бы поле было не final, мы бы потеряли всю логику работы
        //assertNotEquals(oldHashCode, key.hashCode()); //пример без final

        assertNotNull(myObjectMap.get(key));
        assertNotNull(myObjectMap.get(new MyObject(1)));

        //Если key.hashCode выдает константу или разброс значений не велик, допустим от 0 до 10, то теряем в производительности
        //структуры хеш-таблиц

        //Если key.equals непредсказуемый результат выдает, то будут дубли и потеря значений.



        //Хотели обновить значение
        myObjectMap.put(new MyObject(2), 2);

        //Хотели обновить значение
        assertEquals((Integer)1, myObjectMap.get(key));
    }


    @Test
    public void sortedMap(){
        SortedMap<String, Integer> map = new TreeMap<>();

        map.put("aaac", 3);
        map.put("aaaa", 1);
        map.put("aaab", 2);


        assertEquals("aaaa", map.firstKey());
        assertEquals("aaac", map.lastKey());

        System.out.println(map.keySet());
        System.out.println(map.values());
        System.out.println(map.entrySet());

    }


    static class MyObject {
        final int id;

        public MyObject(int id) {
            this.id = id;
        }

        @Override
        public boolean equals(Object o) {
            if (this == o) return true;
            if (o == null || getClass() != o.getClass()) return false;
            MyObject myObject = (MyObject) o;
            return id == myObject.id;
        }

        @Override
        public int hashCode() {
            return Objects.hash(id);
        }
    }
}
