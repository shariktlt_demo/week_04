import org.junit.Test;

import java.util.*;

import static org.junit.Assert.*;

public class ComparatorAndComparable {

    @Test
    public void comparator() {
        List<SomeElement> list = new ArrayList<>();

        list.add(new SomeElement(3, 3));
        list.add(new SomeElement(3, 2));
        list.add(new SomeElement(3, 1));
        list.add(new SomeElement(1, 1));
        list.add(new SomeElement(2, 2));

        Comparator<SomeElement> idAndAgeAsc = getSomeElementComparator(1);
        list.sort(idAndAgeAsc);
        System.out.println(list);

        list.sort(getSomeElementAgeComparator(-1));
        System.out.println(list);


        assertEquals(-1, idAndAgeAsc.compare(new SomeElement(1, 1), new SomeElement(1, 2)));
        assertEquals(0, idAndAgeAsc.compare(new SomeElement(1, 1), new SomeElement(1, 1)));
        assertEquals(1, idAndAgeAsc.compare(new SomeElement(1, 2), new SomeElement(1, 1)));
        assertEquals(1, idAndAgeAsc.compare(new SomeElement(2, 1), new SomeElement(1, 1)));

    }

    @Test
    public void sortedSet(){
        //SomeElement реализует Comparable, можем не передавать Comparator в конструктор
        SortedSet<SomeElement> set = new TreeSet<>();

        //OtherElement НЕ реализует Comparable, передача Comparator<OtherElement> обязательна
        SortedSet<OtherElement> otherSet = new TreeSet<>(((o1, o2) -> 0)); //пустой компаратор для примера

        set = new TreeSet<>(((o1, o2) -> {
            if(o1.id == o2.id){
                return 0;
            }
            return o1.id < o2.id ? -1 : 1;
        } ));

        //Переопределение логики встроенной в метод compareTo в классе SomeElement
        assertTrue(set.add(new SomeElement(1, 1)));

        //Проверяем наличие в коллекции
        assertTrue(set.contains(new SomeElement(1, 1)));
        assertTrue(set.contains(new SomeElement(1, 2)));
        assertTrue(set.contains(new SomeElement(1, 3)));

        //Не добавляем дубликаты в коллекцию - получаем false
        assertFalse(set.add(new SomeElement(1, 1)));
        assertFalse(set.add(new SomeElement(1, 2)));
        assertFalse(set.add(new SomeElement(1, 3)));
    }

    private Comparator<SomeElement> getSomeElementComparator(int direction) {
        return (current, compare) -> {
            if (current.id == compare.id && current.age == compare.age) {
                return 0;
            }
            if (current.id != compare.id) {
                return (current.id < compare.id ? -1 : 1) * direction;
            }
            return (current.age < compare.age ? -1 : 1) * direction;
        };
    }

    private Comparator<SomeElement> getSomeElementAgeComparator(int direction) {
        return (current, compare) -> {
            if ( current.age == compare.age) {
                return 0;
            }
            return (current.age < compare.age ? -1 : 1) * direction;
        };
    }

    private static class OtherElement {
        int id;
        int age;
    }

    private static class SomeElement implements Comparable<SomeElement>{
        int id;
        int age;

        Comparator<SomeElement> comparator;

        public SomeElement(int id, int age) {
            this.id = id;
            this.age = age;
        }

        public void setComparator(Comparator<SomeElement> comparator) {
            this.comparator = comparator;
        }

        @Override
        public boolean equals(Object o) {
            if (this == o) return true;
            if (o == null || getClass() != o.getClass()) return false;
            SomeElement that = (SomeElement) o;
            return id == that.id;
        }

        @Override
        public int hashCode() {
            return Objects.hash(id);
        }

        @Override
        public String toString() {
            return "SomeElement{" +
                    "id=" + id +
                    ", age=" + age +
                    '}';
        }

        /**
         * Compares this object with the specified object for order.  Returns a
         * negative integer, zero, or a positive integer as this object is less
         * than, equal to, or greater than the specified object.
         *
         * <p>The implementor must ensure
         * {@code sgn(x.compareTo(y)) == -sgn(y.compareTo(x))}
         * for all {@code x} and {@code y}.  (This
         * implies that {@code x.compareTo(y)} must throw an exception iff
         * {@code y.compareTo(x)} throws an exception.)
         *
         * <p>The implementor must also ensure that the relation is transitive:
         * {@code (x.compareTo(y) > 0 && y.compareTo(z) > 0)} implies
         * {@code x.compareTo(z) > 0}.
         *
         * <p>Finally, the implementor must ensure that {@code x.compareTo(y)==0}
         * implies that {@code sgn(x.compareTo(z)) == sgn(y.compareTo(z))}, for
         * all {@code z}.
         *
         * <p>It is strongly recommended, but <i>not</i> strictly required that
         * {@code (x.compareTo(y)==0) == (x.equals(y))}.  Generally speaking, any
         * class that implements the {@code Comparable} interface and violates
         * this condition should clearly indicate this fact.  The recommended
         * language is "Note: this class has a natural ordering that is
         * inconsistent with equals."
         *
         * <p>In the foregoing description, the notation
         * {@code sgn(}<i>expression</i>{@code )} designates the mathematical
         * <i>signum</i> function, which is defined to return one of {@code -1},
         * {@code 0}, or {@code 1} according to whether the value of
         * <i>expression</i> is negative, zero, or positive, respectively.
         *
         * @param o the object to be compared.
         * @return a negative integer, zero, or a positive integer as this object
         * is less than, equal to, or greater than the specified object.
         * @throws NullPointerException if the specified object is null
         * @throws ClassCastException   if the specified object's type prevents it
         *                              from being compared to this object.
         */
        @Override
        public int compareTo(SomeElement o) {
            return comparator.compare(this, o);
        }
    }
}
